package com.portafolio.turismoReal.message.request;



public class DepartamentoForm {

	private Long id;
	private String direccion;
	private String caracteristicas;
	private int valorNoche;
	private int dormitorios;
	private int camas;
	private int estado;
	private int comuna;
	
	
	public DepartamentoForm() {
		
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public String getCaracteristicas() {
		return caracteristicas;
	}


	public void setCaracteristicas(String caracteristicas) {
		this.caracteristicas = caracteristicas;
	}


	public int getValorNoche() {
		return valorNoche;
	}


	public void setValorNoche(int valorNoche) {
		this.valorNoche = valorNoche;
	}


	public int getDormitorios() {
		return dormitorios;
	}


	public void setDormitorios(int dormitorios) {
		this.dormitorios = dormitorios;
	}

	public int getEstado() {
		return estado;
	}


	public void setEstado(int estado) {
		this.estado = estado;
	}


	public int getCamas() {
		return camas;
	}


	public void setCamas(int camas) {
		this.camas = camas;
	}


	public int getComuna() {
		return comuna;
	}


	public void setComuna(int comuna) {
		this.comuna = comuna;
	}
	
	
	
}

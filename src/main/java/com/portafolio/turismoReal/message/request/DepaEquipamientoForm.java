package com.portafolio.turismoReal.message.request;

public class DepaEquipamientoForm {
	private long id;
	private long idEquipamiento;

	public DepaEquipamientoForm() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getIdEquipamiento() {
		return idEquipamiento;
	}

	public void setIdEquipamiento(long idEquipamiento) {
		this.idEquipamiento = idEquipamiento;
	}

}

package com.portafolio.turismoReal.message.request;




public class RegistroForm {

	private long id;
	
    private String nombre;

   
    private String username;

    
    private String correo;
    
    private String perfil;
    
  
    private String password;
    
   
    private int habilitado;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
    

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getPerfil() {
    	return this.perfil;
    }
    
    public void setPerfil(String perfil) {
    	this.perfil = perfil;
    }

	public int getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(int habilitado) {
		this.habilitado = habilitado;
	}

	
	@Override
	public String toString() {
		return "RegistroForm [id=" + id + ", nombre=" + nombre + ", username=" + username + ", correo=" + correo
				+ ", perfil=" + perfil + ", password=" + password + "]";
	}
    
    

}

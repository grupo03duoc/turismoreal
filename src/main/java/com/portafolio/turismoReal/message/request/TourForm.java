package com.portafolio.turismoReal.message.request;

public class TourForm {

	private long id;
	private String nombre;
	private String descripcion;
	private int valor_persona;
	private long region;
	
	public TourForm() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getValor_persona() {
		return valor_persona;
	}

	public void setValor_persona(int valor_persona) {
		this.valor_persona = valor_persona;
	}

	public long getRegion() {
		return region;
	}

	public void setRegion(long region) {
		this.region = region;
	}
	
	
}

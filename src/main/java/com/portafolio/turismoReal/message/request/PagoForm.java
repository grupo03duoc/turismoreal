package com.portafolio.turismoReal.message.request;


public class PagoForm {

	private long id;
	private String fecha;
	private int monto;
	private long tipoPago;
	private long reserva;
	
	public PagoForm() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public int getMonto() {
		return monto;
	}

	public void setMonto(int monto) {
		this.monto = monto;
	}

	public long getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(long tipoPago) {
		this.tipoPago = tipoPago;
	}

	public long getReserva() {
		return reserva;
	}

	public void setReserva(long reserva) {
		this.reserva = reserva;
	}

	
	
	
}

package com.portafolio.turismoReal.message.request;


public class TrasladoForm {
	private long id;
    private String fecha;
    private String ubicacion;
    private long chofer;
    private long reserva;
    
    public TrasladoForm() {
    	
    }

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public long getChofer() {
		return chofer;
	}

	public void setChofer(long chofer) {
		this.chofer = chofer;
	}

	public long getReserva() {
		return reserva;
	}

	public void setReserva(long reserva) {
		this.reserva = reserva;
	}
    
    
}

package com.portafolio.turismoReal.message.request;

public class ReservaTourForm {

	private long reserva;
	private long tour;
	
	public ReservaTourForm() {
		
	}

	public long getReserva() {
		return reserva;
	}

	public void setReserva(long reserva) {
		this.reserva = reserva;
	}

	public long getTour() {
		return tour;
	}

	public void setTour(long tour) {
		this.tour = tour;
	}
	
	
}

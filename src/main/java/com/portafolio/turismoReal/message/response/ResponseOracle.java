package com.portafolio.turismoReal.message.response;

public class ResponseOracle {
	
	private int estado;
	private String glosa;
	private int id;
	
	public ResponseOracle() {
		
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	public String getGlosa() {
		return glosa;
	}
	public void setGlosa(String glosa) {
		this.glosa = glosa;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	
	

}

package com.portafolio.turismoReal.repository;

import java.util.*;
import oracle.jdbc.OracleTypes;
import com.portafolio.turismoReal.model.Region;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

@Repository
public class RegionRepository {

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	   @SuppressWarnings("unchecked")
    public List<Region> AllRegiones() {
        List<Region> regiones = new ArrayList<>();
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_REGIONES");

        jdbcCall.declareParameters(new SqlOutParameter("OUT_PC_GET_REG", OracleTypes.CURSOR),
                new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
                new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));
        Map<String, Object> result = jdbcCall.execute();
        List<Map<String, Object>> recordset = (List<Map<String, Object>>) result.get("OUT_PC_GET_REG");
        for (Map<String, Object> obj : recordset) {
            Region region = new Region();
            region.setId(Long.parseLong(obj.get("REGION_ID").toString()));
            region.setNombre(obj.get("REGION_NOMBRE").toString());
            region.setAbreviatura(obj.get("ABREVIATURA").toString());
            region.setCapital(obj.get("CAPITAL").toString());
            regiones.add(region);
        }

        return regiones;
    }
	
	@SuppressWarnings("unchecked")
	public Region GetRegion(long id){
		 Region region = new Region();
	       SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_REGION_X_ID");

			jdbcCall.declareParameters(new SqlParameter("IN_REGION_ID", OracleTypes.NUMBER),
					                   new SqlOutParameter("OUT_PC_GET_REG", OracleTypes.CURSOR),
					                   new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
                                       new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("IN_REGION_ID", id);
			
			Map<String,Object> result = jdbcCall.execute(params);
			
			List<Map<String, Object>> recordset = (List<Map<String,  Object>>) result.get("OUT_PC_GET_REG");
			 for( Map<String,  Object> obj: recordset) {
	        	 region.setId(Long.parseLong(obj.get("REGION_ID").toString()));
	        	 region.setNombre(obj.get("REGION_NOMBRE").toString());
	        	 region.setAbreviatura(obj.get("ABREVIATURA").toString());
	        	 region.setCapital(obj.get("CAPITAL").toString());
			 }
 
			return region;
	}
}

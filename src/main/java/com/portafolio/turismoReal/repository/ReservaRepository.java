package com.portafolio.turismoReal.repository;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.portafolio.turismoReal.message.response.ResponseOracle;
import com.portafolio.turismoReal.model.Reserva;

import oracle.jdbc.OracleTypes;

@Repository
public class ReservaRepository {

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	public ResponseOracle create(Reserva reserva) {
		ResponseOracle response = new ResponseOracle();
		Date fechaInicio = new Date();
		Date fechaTermino = new Date();
		try {
			 fechaInicio= new SimpleDateFormat("dd-MM-yyyy").parse(reserva.getFechaInicio());
			 fechaTermino= new SimpleDateFormat("dd-MM-yyyy").parse(reserva.getFechaTermino());
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_CREAR_RESERVA");
		 jdbcCall.declareParameters(
                 new SqlParameter("IN_FECHA_INICIO", OracleTypes.DATE),
                 new SqlParameter("IN_FECHA_TERMINO", OracleTypes.DATE),
                 new SqlParameter("IN_CANT_PERSONAS", OracleTypes.NUMBER),
                 new SqlParameter("IN_MONTO_TOTAL", OracleTypes.NUMBER),
                 new SqlParameter("IN_USUARIO_ID", OracleTypes.NUMBER),
                 new SqlParameter("IN_DEPARTAMENTO_ID", OracleTypes.NUMBER),
                 new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
                 new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER),
                 new SqlOutParameter("OUT_ID", OracleTypes.NUMBER));
		 
		 Map<String, Object> params=new HashMap<String, Object>();
	  	 params.put("IN_FECHA_INICIO",fechaInicio);
	  	 params.put("IN_FECHA_TERMINO",fechaTermino);
	  	 params.put("IN_CANT_PERSONAS",reserva.getPersonas());
	  	 params.put("IN_MONTO_TOTAL", reserva.getTotal());
	  	 params.put("IN_USUARIO_ID", reserva.getUsuario());
	  	 params.put("IN_DEPARTAMENTO_ID", reserva.getDepartamento());
	  	 
	  	 Map<String, Object> result = jdbcCall.execute(params);
	  	 
		 response.setEstado(Integer.valueOf(result.get("OUT_ESTADO").toString()));
		 response.setGlosa(result.get("OUT_GLOSA").toString());
		 response.setId(Integer.valueOf(result.get("OUT_ID").toString()));
		 
		return response;
	}
	
	@SuppressWarnings("unchecked")
	public Reserva getReserva(Long id) {
	   Reserva reserva = new Reserva();
	   DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_RESERVAS_X_ID");
		jdbcCall.declareParameters(new SqlParameter("IN_RES_ID", OracleTypes.NUMERIC),
				new SqlOutParameter("OUT_PC_GET_RES_ID", OracleTypes.CURSOR),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IN_RES_ID", id);

		Map<String, Object> result = jdbcCall.execute(params);

		List<Map<String, Object>> recordset = (List<Map<String, Object>>) result.get("OUT_PC_GET_RES_ID");
		for (Map<String, Object> obj : recordset) {
			reserva.setId(Long.parseLong(obj.get("RESERVA_ID").toString()));
			Object fecha_Inicio = obj.get("RESERVA_FECHA_INICIO");
			Object fecha_Termino = obj.get("RESERVA_FECHA_TERMINO");
			try {
				reserva.setFechaInicio(dateFormat.format(formatter.parse(fecha_Inicio.toString())));
				reserva.setFechaTermino(dateFormat.format(formatter.parse(fecha_Termino.toString())));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			reserva.setPersonas(Integer.valueOf(obj.get("RESERVA_CANT_PERSONAS").toString()));
			reserva.setTotal(Integer.valueOf(obj.get("RESERVA_MONTO_TOTAL").toString()));
			reserva.setUsuario(Long.parseLong(obj.get("USUARIO_ID").toString()));
			reserva.setDepartamento(Long.parseLong(obj.get("DEPARTAMENTO_ID").toString()));
		}
	   return reserva;
	}
	
	@SuppressWarnings("unchecked")
	public List<Reserva> getAllReservaUsuario(Long idUsuario){
		List<Reserva> reservas = new ArrayList<>();
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_RESERVAS_X_ID_CL");
		jdbcCall.declareParameters(new SqlParameter("IN_RES_ID", OracleTypes.NUMERIC),
				new SqlOutParameter("OUT_PC_GET_RES_CL", OracleTypes.CURSOR),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IN_RES_ID", idUsuario);

		Map<String, Object> result = jdbcCall.execute(params);

		List<Map<String, Object>> recordset = (List<Map<String, Object>>) result.get("OUT_PC_GET_RES_CL");
		for (Map<String, Object> obj : recordset) {
			Reserva reserva = new Reserva();
			reserva.setId(Long.parseLong(obj.get("RESERVA_ID").toString()));
			Object fecha_Inicio = obj.get("RESERVA_FECHA_INICIO");
			Object fecha_Termino = obj.get("RESERVA_FECHA_TERMINO");
			try {
				reserva.setFechaInicio(dateFormat.format(formatter.parse(fecha_Inicio.toString())));
				reserva.setFechaTermino(dateFormat.format(formatter.parse(fecha_Termino.toString())));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			reserva.setPersonas(Integer.valueOf(obj.get("RESERVA_CANT_PERSONAS").toString()));
			reserva.setTotal(Integer.valueOf(obj.get("RESERVA_MONTO_TOTAL").toString()));
			reserva.setUsuario(Long.parseLong(obj.get("USUARIO_ID").toString()));
			reserva.setDepartamento(Long.parseLong(obj.get("DEPARTAMENTO_ID").toString()));
			reservas.add(reserva);
		}
		return reservas;
	}
	
	@SuppressWarnings("unchecked")
	public List<Reserva> getAllReservaDepartamento(long id){
		List<Reserva> reservas = new ArrayList<>();
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_RESERVAS_X_ID_DEP");
		jdbcCall.declareParameters(new SqlParameter("IN_DEP_ID", OracleTypes.NUMERIC),
				new SqlOutParameter("OUT_PC_GET_RES_DEP", OracleTypes.CURSOR),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IN_DEP_ID", id);

		Map<String, Object> result = jdbcCall.execute(params);

		List<Map<String, Object>> recordset = (List<Map<String, Object>>) result.get("OUT_PC_GET_RES_DEP");
		for (Map<String, Object> obj : recordset) {
			Reserva reserva = new Reserva();
			reserva.setId(Long.parseLong(obj.get("RESERVA_ID").toString()));
			Object fecha_Inicio = obj.get("RESERVA_FECHA_INICIO");
			Object fecha_Termino = obj.get("RESERVA_FECHA_TERMINO");
			try {
				reserva.setFechaInicio(dateFormat.format(formatter.parse(fecha_Inicio.toString())));
				reserva.setFechaTermino(dateFormat.format(formatter.parse(fecha_Termino.toString())));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			reserva.setPersonas(Integer.valueOf(obj.get("RESERVA_CANT_PERSONAS").toString()));
			reserva.setTotal(Integer.valueOf(obj.get("RESERVA_MONTO_TOTAL").toString()));
			reserva.setUsuario(Long.parseLong(obj.get("USUARIO_ID").toString()));
			reserva.setDepartamento(Long.parseLong(obj.get("DEPARTAMENTO_ID").toString()));
			reservas.add(reserva);
		}
		return reservas;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Reserva> getAll(){
		List<Reserva> reservas = new ArrayList<>();
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_RESERVAS");
		jdbcCall.declareParameters(
				new SqlOutParameter("OUT_PC_GET_RES_CL", OracleTypes.CURSOR),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));



		Map<String, Object> result = jdbcCall.execute();

		List<Map<String, Object>> recordset = (List<Map<String, Object>>) result.get("OUT_PC_GET_RES");
		for (Map<String, Object> obj : recordset) {
			Reserva reserva = new Reserva();
			reserva.setId(Long.parseLong(obj.get("RESERVA_ID").toString()));
			Object fecha_Inicio = obj.get("RESERVA_FECHA_INICIO");
			Object fecha_Termino = obj.get("RESERVA_FECHA_TERMINO");
			try {
				reserva.setFechaInicio(dateFormat.format(formatter.parse(fecha_Inicio.toString())));
				reserva.setFechaTermino(dateFormat.format(formatter.parse(fecha_Termino.toString())));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			reserva.setPersonas(Integer.valueOf(obj.get("RESERVA_CANT_PERSONAS").toString()));
			reserva.setTotal(Integer.valueOf(obj.get("RESERVA_MONTO_TOTAL").toString()));
			reserva.setUsuario(Long.parseLong(obj.get("USUARIO_ID").toString()));
			reserva.setDepartamento(Long.parseLong(obj.get("DEPARTAMENTO_ID").toString()));
			reservas.add(reserva);
		}
		return reservas;
	}

	@SuppressWarnings("unchecked")
	public List<Reserva> getAllReservaPorPeriodo(String fechaIni, String fechaTer,long id){
		List<Reserva> reservas = new ArrayList<>();
		Date fechaInicio = new Date();
		Date fechaTermino = new Date();
		try {
			 fechaInicio= new SimpleDateFormat("dd-MM-yyyy").parse(fechaIni);
			 fechaTermino= new SimpleDateFormat("dd-MM-yyyy").parse(fechaTer);
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_RESERVAS_X_PERIODOS");
		jdbcCall.declareParameters(
				new SqlParameter("IN_REGION_ID", OracleTypes.NUMBER),
				new SqlParameter("IN_PERIODO_INICIO", OracleTypes.DATE),
				new SqlParameter("IN_PERIODO_TERMINO", OracleTypes.DATE),
				new SqlOutParameter("OUT_PC_GET_RES", OracleTypes.CURSOR),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IN_REGION_ID", id);
		params.put("IN_PERIODO_INICIO", fechaInicio);
		params.put("IN_PERIODO_TERMINO", fechaTermino);

		Map<String, Object> result = jdbcCall.execute(params);

		List<Map<String, Object>> recordset = (List<Map<String, Object>>) result.get("OUT_PC_GET_RES");
		for (Map<String, Object> obj : recordset) {
			Reserva reserva = new Reserva();
			reserva.setId(Long.parseLong(obj.get("RESERVA_ID").toString()));
			Object fecha_Inicio = obj.get("RESERVA_FECHA_INICIO");
			Object fecha_Termino = obj.get("RESERVA_FECHA_TERMINO");
			try {
				reserva.setFechaInicio(dateFormat.format(formatter.parse(fecha_Inicio.toString())));
				reserva.setFechaTermino(dateFormat.format(formatter.parse(fecha_Termino.toString())));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			reserva.setPersonas(Integer.valueOf(obj.get("RESERVA_CANT_PERSONAS").toString()));
			reserva.setTotal(Integer.valueOf(obj.get("RESERVA_MONTO_TOTAL").toString()));
			reserva.setUsuario(Long.parseLong(obj.get("USUARIO_ID").toString()));
			reserva.setDepartamento(Long.parseLong(obj.get("DEPARTAMENTO_ID").toString()));
			reservas.add(reserva);
		}
		return reservas;
	}
	
}

package com.portafolio.turismoReal.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.portafolio.turismoReal.message.request.ChoferForm;
import com.portafolio.turismoReal.message.response.ResponseOracle;
import com.portafolio.turismoReal.model.Chofer;
import com.portafolio.turismoReal.model.Empresa;

import oracle.jdbc.OracleTypes;

@Repository
public class ChoferRepository {

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	public ResponseOracle create(ChoferForm chofer) {
		ResponseOracle response = new ResponseOracle();
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_CREAR_CHOFER");
		jdbcCall.declareParameters(new SqlParameter("IN_CHOFER_IN_NOM_CHOFER", OracleTypes.VARCHAR),
				new SqlParameter("IN_CHOFER_IN_RUT", OracleTypes.VARCHAR),
				new SqlParameter("IN_CHOFER_VEHICULO", OracleTypes.VARCHAR),
				new SqlParameter("IN_CHOFER_IN_EMPRESA_ID", OracleTypes.NUMBER),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IN_CHOFER_IN_NOM_CHOFER", chofer.getNombre());
		params.put("IN_CHOFER_IN_RUT", chofer.getRut());
		params.put("IN_CHOFER_VEHICULO", chofer.getVehiculo());
		params.put("IN_CHOFER_IN_EMPRESA_ID", chofer.getEmpresa());

		Map<String, Object> result = jdbcCall.execute(params);

		response.setEstado(Integer.valueOf(result.get("OUT_ESTADO").toString()));
		response.setGlosa(result.get("OUT_GLOSA").toString());
		return response;
	}
	
	public ResponseOracle update(ChoferForm chofer) {
		ResponseOracle response = new ResponseOracle();
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_UPDATE_CHOFER")
				.withoutProcedureColumnMetaDataAccess();
		jdbcCall.declareParameters(
				new SqlParameter("IN_CHOFER_ID", OracleTypes.NUMBER),
				new SqlParameter("IN_CHOFER_NOMBRE", OracleTypes.VARCHAR),
				new SqlParameter("IN_CHOFER_RUT", OracleTypes.VARCHAR),
				new SqlParameter("IN_CHOFER_VEHICULO", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IN_CHOFER_ID", chofer.getId());
		params.put("IN_CHOFER_NOMBRE", chofer.getNombre());
		params.put("IN_CHOFER_RUT", chofer.getRut());
		params.put("IN_CHOFER_VEHICULO", chofer.getVehiculo());

		Map<String, Object> result = jdbcCall.execute(params);

		response.setEstado(Integer.valueOf(result.get("OUT_ESTADO").toString()));
		response.setGlosa(result.get("OUT_GLOSA").toString());
		return response;
	}
	
	@SuppressWarnings("unchecked")
	public List<Chofer> getChoferPorEmpresa(long id) {
		List<Chofer> choferes = new ArrayList<>();
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_CHOFER_X_EMP_ID");
		jdbcCall.declareParameters(new SqlParameter("IN_EMP_ID", OracleTypes.NUMERIC),
				new SqlOutParameter("OUT_PC_GET_CHOF", OracleTypes.CURSOR),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IN_EMP_ID", id);

		Map<String, Object> result = jdbcCall.execute(params);

		List<Map<String, Object>> recordset = (List<Map<String, Object>>) result.get("OUT_PC_GET_CHOF");
		for (Map<String, Object> obj : recordset) {
			Chofer chofer = new Chofer();
			chofer.setId(Long.parseLong(obj.get("CHOFER_ID").toString()));
			chofer.setNombre(obj.get("CHOFER_NOMBRE").toString());
			chofer.setVehiculo(obj.get("CHOFER_VEHICULO").toString());
			chofer.setRut(obj.get("CHOFER_RUT").toString());
			Empresa empresa = new Empresa();
			empresa.setId(Long.parseLong(obj.get("EMPRESA_ID").toString()));
			empresa.setNombre(obj.get("EMPRESA_NOMBRE").toString());
			chofer.setEmpresa(empresa);
			choferes.add(chofer);
		}
		return choferes;
	}

}

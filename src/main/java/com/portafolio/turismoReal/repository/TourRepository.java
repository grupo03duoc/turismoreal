package com.portafolio.turismoReal.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.portafolio.turismoReal.message.request.ReservaTourForm;
import com.portafolio.turismoReal.message.request.TourForm;
import com.portafolio.turismoReal.message.response.ResponseOracle;
import com.portafolio.turismoReal.model.Tour;

import oracle.jdbc.OracleTypes;

@Repository
public class TourRepository {

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	public ResponseOracle create(TourForm tour) {
		ResponseOracle response = new ResponseOracle();
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_CREAR_TOUR");
    	jdbcCall.declareParameters(
    			                   new SqlParameter("IN_TOUR_NOMBRE", OracleTypes.VARCHAR),
    			                   new SqlParameter("IN_TOUR_DESC", OracleTypes.VARCHAR),
    			                   new SqlParameter("IN_TOUR_VALOR", OracleTypes.NUMBER),
    			                   new SqlParameter("IN_TOUR_REGION_ID", OracleTypes.NUMBER),
    			                   new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
    			                   new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));
    	
      Map<String, Object> params=new HashMap<String, Object>();
  	  params.put("IN_TOUR_NOMBRE",tour.getNombre());
  	  params.put("IN_TOUR_DESC",tour.getDescripcion());
  	  params.put("IN_TOUR_VALOR", tour.getValor_persona());
  	  params.put("IN_TOUR_REGION_ID",tour.getRegion());
  	  
  	  Map<String, Object> result = jdbcCall.execute(params);
  	  
	  response.setEstado(Integer.valueOf(result.get("OUT_ESTADO").toString()));
	  response.setGlosa(result.get("OUT_GLOSA").toString());
	  
	  return response;
	}
	
	public ResponseOracle createReservaTour(ReservaTourForm reservaTour) {
		ResponseOracle response = new ResponseOracle();
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_CREAR_RES_TOUR");
    	jdbcCall.declareParameters(
    			                   new SqlParameter("IN_RESERVA_ID", OracleTypes.NUMBER),
    			                   new SqlParameter("IN_TOUR_ID", OracleTypes.NUMBER),
    			                   new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
    			                   new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));
    	
      Map<String, Object> params=new HashMap<String, Object>();
      
      params.put("IN_RESERVA_ID",reservaTour.getReserva());
  	  params.put("IN_TOUR_ID",reservaTour.getTour());
  	  
  	  Map<String, Object> result = jdbcCall.execute(params);
	  
	  response.setEstado(Integer.valueOf(result.get("OUT_ESTADO").toString()));
	  response.setGlosa(result.get("OUT_GLOSA").toString());
	  
	  return response;
	}
	
	public ResponseOracle update(TourForm tour) {
		ResponseOracle response = new ResponseOracle();
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_UPDATE_TOUR");
    	jdbcCall.declareParameters(
    			                   new SqlParameter("IN_TOUR_ID", OracleTypes.NUMBER),
    			                   new SqlParameter("IN_TOUR_NOMBRE", OracleTypes.VARCHAR),
    			                   new SqlParameter("IN_TOUR_DESC", OracleTypes.VARCHAR),
    			                   new SqlParameter("IN_TOUR_VALOR_PER", OracleTypes.NUMBER),
    			                   new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
    			                   new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));
    	
      Map<String, Object> params=new HashMap<String, Object>();
      params.put("IN_TOUR_ID",tour.getId());
  	  params.put("IN_TOUR_NOMBRE",tour.getNombre());
  	  params.put("IN_TOUR_DESC",tour.getDescripcion());
  	  params.put("IN_TOUR_VALOR_PER", tour.getValor_persona());
  	  
  	  Map<String, Object> result = jdbcCall.execute(params);
  	  
	  response.setEstado(Integer.valueOf(result.get("OUT_ESTADO").toString()));
	  response.setGlosa(result.get("OUT_GLOSA").toString());
	  
	  return response;
	}
	
	@SuppressWarnings("unchecked")
	public List<Tour> getAllTourPorRegion(long id){
		List<Tour> tours = new ArrayList<>();
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_TOUR_X_COM_ID");
		jdbcCall.declareParameters(new SqlParameter("IN_COM_ID", OracleTypes.NUMERIC),
				new SqlOutParameter("OUT_PC_GET_COM_ID", OracleTypes.CURSOR),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IN_COM_ID", id);

		Map<String, Object> result = jdbcCall.execute(params);

		List<Map<String, Object>> recordset = (List<Map<String, Object>>) result.get("OUT_PC_GET_COM_ID");
		for (Map<String, Object> obj : recordset) {
			Tour tour = new Tour();
			tour.setId(Long.parseLong(obj.get("TOUR_ID").toString()));
			tour.setNombre(obj.get("TOUR_NOMBRE").toString());
			tour.setDescripcion(obj.get("TOUR_DESCRIPCION").toString());
			tour.setValor_persona(Integer.valueOf(obj.get("TOUR_VALOR_PERSONA").toString()));
			tour.setRegion(obj.get("REGION_NOMBRE").toString());	
			tours.add(tour);
		}
		return tours;
	}
	
	@SuppressWarnings("unchecked")
	public Tour getTourPorReserva(long id){
		Tour tour = new Tour();
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_TOUR_X_RES");
		jdbcCall.declareParameters(new SqlParameter("IN_RES_ID", OracleTypes.NUMERIC),
				new SqlOutParameter("OUT_PC_GET_TOUR", OracleTypes.CURSOR),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IN_RES_ID", id);

		Map<String, Object> result = jdbcCall.execute(params);

		List<Map<String, Object>> recordset = (List<Map<String, Object>>) result.get("OUT_PC_GET_TOUR");
		for (Map<String, Object> obj : recordset) {
			tour.setId(Long.parseLong(obj.get("TOUR_ID").toString()));
			tour.setNombre(obj.get("TOUR_NOMBRE").toString());
			tour.setDescripcion(obj.get("TOUR_DESCRIPCION").toString());
			tour.setValor_persona(Integer.valueOf(obj.get("TOUR_VALOR_PERSONA").toString()));
			tour.setRegion(obj.get("REGION_NOMBRE").toString());	
		}
		return tour;
	}
}

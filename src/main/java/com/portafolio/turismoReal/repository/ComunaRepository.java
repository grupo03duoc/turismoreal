package com.portafolio.turismoReal.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.portafolio.turismoReal.model.Comuna;

import oracle.jdbc.OracleTypes;

@Repository
public class ComunaRepository {

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@SuppressWarnings("unchecked")
	public List<Comuna> AllComunas(Long idProvincia){
		 List<Comuna> comunas = new ArrayList<>();
	       SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_COM_PRV_ID");

			jdbcCall.declareParameters(new SqlParameter("IN_PROV_ID", OracleTypes.NUMERIC),
					                   new SqlOutParameter("OUT_PC_GET_COM", OracleTypes.CURSOR),
					                   new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
                                       new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));
			  Map<String, Object> params=new HashMap<String, Object>();
	    	  params.put("IN_PROV_ID", idProvincia);
	    	  
			Map<String, Object> result = jdbcCall.execute(params);
			List<Map<String, Object>> recordset = (List<Map<String, Object>>) result.get("OUT_PC_GET_COM");
			 for( Map<String, Object> obj: recordset) {
				 Comuna comuna = new Comuna();
	        	 comuna.setId(Long.parseLong(obj.get("COMUNA_ID").toString()));
	        	 comuna.setNombre(obj.get("COMUNA_NOMBRE").toString());
	        	 comunas.add(comuna);
			 }
 
			return comunas;
	}
}

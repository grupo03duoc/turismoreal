package com.portafolio.turismoReal.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.portafolio.turismoReal.message.request.DepartamentoForm;
import com.portafolio.turismoReal.message.response.ResponseOracle;
import com.portafolio.turismoReal.model.Comuna;
import com.portafolio.turismoReal.model.Departamento;

import oracle.jdbc.OracleTypes;

@Repository
public class DepartamentoRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public ResponseOracle create(DepartamentoForm depa) {
		ResponseOracle response = new ResponseOracle();
		depa.setEstado(1);
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_INSERT_DEPTO")
				.withoutProcedureColumnMetaDataAccess();
		jdbcCall.declareParameters(new SqlParameter("IN_DEP_DIR", OracleTypes.VARCHAR),
				new SqlParameter("IN_DEP_CARACT", OracleTypes.VARCHAR),
				new SqlParameter("IN_DEP_VALOR_NO", OracleTypes.NUMBER),
				new SqlParameter("IN_DEP_IN_COM_ID", OracleTypes.NUMBER),
				new SqlParameter("IN_DEP_CANT_DOR", OracleTypes.NUMBER),
				new SqlParameter("IN_DEP_CANT_CAM", OracleTypes.NUMBER),
				new SqlParameter("IN_DEP_ESTADO", OracleTypes.NUMBER),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IN_DEP_DIR", depa.getDireccion());
		params.put("IN_DEP_CARACT", depa.getCaracteristicas());
		params.put("IN_DEP_VALOR_NO", depa.getValorNoche());
		params.put("IN_DEP_IN_COM_ID", depa.getComuna());
		params.put("IN_DEP_CANT_DOR", depa.getDormitorios());
		params.put("IN_DEP_CANT_CAM", depa.getCamas());
		params.put("IN_DEP_CANT_CAM", depa.getEstado());

		Map<String, Object> result = jdbcCall.execute(params);

		response.setEstado(Integer.valueOf(result.get("OUT_ESTADO").toString()));
		response.setGlosa(result.get("OUT_GLOSA").toString());
		return response;
	}

	@SuppressWarnings("unchecked")
	public List<Departamento> AllDepartamentos(Long idComuna) {
		List<Departamento> departamentos = new ArrayList<>();
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_DEPTOS_X_ID_COM");
		jdbcCall.declareParameters(new SqlParameter("IN_ID_COMUNA", OracleTypes.NUMERIC),
				new SqlOutParameter("OUT_PC_GET_DEP_COM", OracleTypes.CURSOR),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IN_ID_COMUNA", idComuna);

		Map<String, Object> result = jdbcCall.execute(params);

		List<Map<String, Object>> recordset = (List<Map<String, Object>>) result.get("OUT_PC_GET_DEP_COM");
		for (Map<String, Object> obj : recordset) {
			Departamento depa = new Departamento();
			depa.setId(Long.parseLong(obj.get("DEPARTAMENTO_ID").toString()));
			depa.setDireccion(obj.get("DEPARTAMENTO_DIRECCION").toString());
			depa.setCaracteristicas(obj.get("DEPARTAMENTO_CARACTERISTICAS").toString());
			depa.setValorNoche(Integer.valueOf(obj.get("DEPARTAMENTO_VALOR_NOCHE").toString()));
			depa.setDormitorios(Integer.valueOf(obj.get("CANT_DORMITORIOS").toString()));
			depa.setCamas(Integer.valueOf(obj.get("CANT_CAMAS").toString()));
			Comuna comuna = new Comuna();
			comuna.setId(Long.parseLong( obj.get("COMUNA_ID").toString()));
			comuna.setNombre(obj.get("COMUNA_NOMBRE").toString());
			depa.setComuna(comuna);
			departamentos.add(depa);
		}

		return departamentos;
	}
	
	@SuppressWarnings("unchecked")
	public Departamento getDepartamentoById(long id) {
		Departamento depa = new Departamento();
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_DEP_X_ID");
		jdbcCall.declareParameters(new SqlParameter("IN_DEP_ID", OracleTypes.NUMERIC),
				new SqlOutParameter("OUT_PC_GET_DEP_ID", OracleTypes.CURSOR),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IN_DEP_ID", id);

		Map<String, Object> result = jdbcCall.execute(params);

		List<Map<String, Object>> recordset = (List<Map<String, Object>>) result.get("OUT_PC_GET_DEP_ID");
		for (Map<String, Object> obj : recordset) {
			depa.setId(Long.parseLong(obj.get("DEPARTAMENTO_ID").toString()));
			depa.setDireccion(obj.get("DEPARTAMENTO_DIRECCION").toString());
			depa.setCaracteristicas(obj.get("DEPARTAMENTO_CARACTERISTICAS").toString());
			depa.setValorNoche(Integer.valueOf(obj.get("DEPARTAMENTO_VALOR_NOCHE").toString()));
			depa.setDormitorios(Integer.valueOf(obj.get("CANT_DORMITORIOS").toString()));
			depa.setCamas(Integer.valueOf(obj.get("CANT_CAMAS").toString()));
			Comuna comuna = new Comuna();
			comuna.setId(Long.parseLong(obj.get("COMUNA_ID").toString()));
			comuna.setNombre(obj.get("COMUNA_NOMBRE").toString());
			depa.setComuna(comuna);
		}
		
		return depa;
	}
	
	public ResponseOracle updateDepartamento(DepartamentoForm depa) {
		ResponseOracle response = new ResponseOracle();
		
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_UPDATE_DEPARTAMENTO")
				.withoutProcedureColumnMetaDataAccess();
		jdbcCall.declareParameters(
				new SqlParameter("IN_DEPTO_ID", OracleTypes.NUMBER),
				new SqlParameter("IN_DEPTO_DIRECCION", OracleTypes.VARCHAR),
				new SqlParameter("IN_DEPTO_CARACT", OracleTypes.VARCHAR),
				new SqlParameter("IN_DEPTO_VALOR_NOCHE", OracleTypes.NUMBER),
				new SqlParameter("IN_DEPTO_CANT_DORM", OracleTypes.NUMBER),
				new SqlParameter("IN_DEPTO_CANT_CAMAS", OracleTypes.NUMBER),
				new SqlParameter("IN_DEPTO_ESTADO", OracleTypes.NUMBER),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("IN_DEPTO_ID", depa.getId());
		params.put("IN_DEPTO_DIRECCION", depa.getDireccion());
		params.put("IN_DEPTO_CARACT", depa.getCaracteristicas());
		params.put("IN_DEPTO_VALOR_NOCHE", depa.getValorNoche());
		params.put("IN_DEPTO_CANT_DORM", depa.getDormitorios());
		params.put("IN_DEPTO_CANT_CAMAS", depa.getCamas());
		params.put("IN_DEPTO_ESTADO", depa.getEstado());

		Map<String, Object> result = jdbcCall.execute(params);

		response.setEstado(Integer.valueOf(result.get("OUT_ESTADO").toString()));
		response.setGlosa(result.get("OUT_GLOSA").toString());
		return response;
	}
	
	public ResponseOracle desactivar(long id, int estado) {
		ResponseOracle response = new ResponseOracle();
		
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_DESACT_DEPARTAMENTO")
				.withoutProcedureColumnMetaDataAccess();
		jdbcCall.declareParameters(
				new SqlParameter("IN_DEPTO_ID", OracleTypes.NUMBER),
				new SqlParameter("IN_DEPTO_ESTADO", OracleTypes.NUMBER),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));
		
		 Map<String, Object> params=new HashMap<String, Object>();
	  	 params.put("IN_DEPTO_ID",id);
	  	 params.put("IN_DEPTO_ESTADO",estado);
	  	 
	  	Map<String, Object> result = jdbcCall.execute(params);
		response.setEstado(Integer.valueOf(result.get("OUT_ESTADO").toString()));
		response.setGlosa(result.get("OUT_GLOSA").toString());
		 
		return response;
	}
	
	

	public ResponseOracle createDepaEquipo(Long idDepa, Long idEquipo) {
		ResponseOracle response = new ResponseOracle();

		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_CREAR_DEP_EQUIP")
				.withoutProcedureColumnMetaDataAccess();
		jdbcCall.declareParameters(new SqlParameter("IN_DEPARTAMENTO_ID", OracleTypes.NUMBER),
				new SqlParameter("IN_EQUIPAMIENTO_ID", OracleTypes.NUMBER),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IN_DEPARTAMENTO_ID", idDepa);
		params.put("IN_EQUIPAMIENTO_ID", idEquipo);

		Map<String, Object> result = jdbcCall.execute(params);

		response.setEstado(Integer.valueOf(result.get("OUT_ESTADO").toString()));
		response.setGlosa(result.get("OUT_GLOSA").toString());

		return response;
	}
}

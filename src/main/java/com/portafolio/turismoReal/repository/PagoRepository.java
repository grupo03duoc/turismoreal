package com.portafolio.turismoReal.repository;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.portafolio.turismoReal.message.request.PagoForm;
import com.portafolio.turismoReal.message.response.ResponseOracle;
import com.portafolio.turismoReal.model.Pago;
import com.portafolio.turismoReal.model.TipoPago;

import oracle.jdbc.OracleTypes;

@Repository
public class PagoRepository {

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	
	public ResponseOracle create(PagoForm pago) {
		Date fecha = new Date();
		try {
			 fecha= new SimpleDateFormat("dd-MM-yyyy").parse(pago.getFecha());
			
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
		ResponseOracle response = new ResponseOracle();
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_CREAR_PAGO").withoutProcedureColumnMetaDataAccess();
		 jdbcCall.declareParameters(
                 new SqlParameter("IN_PAGO_FECHA", OracleTypes.DATE),
                 new SqlParameter("IN_PAGO_MONTO", OracleTypes.NUMBER),
                 new SqlParameter("IN_TIPO_PAGO_ID", OracleTypes.NUMERIC),
                 new SqlParameter("IN_RESERVA_ID", OracleTypes.NUMERIC),
                 new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
                 new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));
		 
		 Map<String, Object> params=new HashMap<String, Object>();
	  	 params.put("IN_PAGO_FECHA",fecha);
	  	 params.put("IN_PAGO_MONTO",pago.getMonto());
	  	 params.put("IN_TIPO_PAGO_ID",pago.getTipoPago());
	  	 params.put("IN_RESERVA_ID", pago.getReserva());
	  	 
	  	 Map<String, Object> result = jdbcCall.execute(params);
		 response.setEstado(Integer.valueOf(result.get("OUT_ESTADO").toString()));
		 response.setGlosa(result.get("OUT_GLOSA").toString());
        
		
		return response;
	}
	
	@SuppressWarnings("unchecked")
	public Pago getPagoPorReserva(long id) {
	   Pago pago = new Pago();
	   DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	   SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_PAGO_X_RES");
		jdbcCall.declareParameters(new SqlParameter("IN_RES_ID", OracleTypes.NUMERIC),
				new SqlOutParameter("OUT_PC_GET_PAGO_X_RES", OracleTypes.CURSOR),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IN_RES_ID", id);

		Map<String, Object> result = jdbcCall.execute(params);

		List<Map<String, Object>> recordset = (List<Map<String, Object>>) result.get("OUT_PC_GET_PAGO_X_RES");
		for (Map<String, Object> obj : recordset) {
			pago.setId(Long.parseLong(obj.get("PAGO_ID").toString()));
			Object fecha = obj.get("PAGO_FECHA");
			try {
				pago.setFecha(dateFormat.format(formatter.parse(fecha.toString())));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			TipoPago tipo = new TipoPago();
			pago.setMonto(Integer.valueOf(obj.get("PAGO_MONTO").toString()));
			pago.setIdRserva(Long.parseLong(obj.get("RESERVA_ID").toString()));
		    tipo.setId(Long.parseLong(obj.get("TIPO_PAGO_ID").toString()));
		    tipo.setDescripcion(obj.get("TIPO_PAGO_DESCRIPCION").toString());
		    pago.setTipoPago(tipo);
			
		}
	   return pago;
	}
}

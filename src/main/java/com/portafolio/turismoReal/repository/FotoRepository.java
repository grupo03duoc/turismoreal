package com.portafolio.turismoReal.repository;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.portafolio.turismoReal.message.response.ResponseOracle;
import com.portafolio.turismoReal.model.Foto;

import oracle.jdbc.OracleTypes;

@Repository
public class FotoRepository {

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	public ResponseOracle create(Foto foto) {
		ResponseOracle response = new ResponseOracle();
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_INSERT_FOTO");
        jdbcCall.declareParameters(
                new SqlParameter("IN_FOTO_RUTA", OracleTypes.VARCHAR),
                new SqlParameter("IN_DEP_ID", OracleTypes.NUMERIC),
                new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
                new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));
        
        Map<String, Object> params= new HashMap<String, Object>();
	  	params.put("IN_FOTO_RUTA",foto.getRuta());
	  	params.put("IN_DEP_ID",foto.getIdDepartamento());
	  	 
	  	Map<String, Object> result = jdbcCall.execute(params);
		 
		 response.setEstado(Integer.valueOf(result.get("OUT_ESTADO").toString()));
		 response.setGlosa(result.get("OUT_GLOSA").toString());
	  	
        
        return response;
	}
	
	@SuppressWarnings("unchecked")
	public List<Foto> allFotos(Long idDepartamento){
		List<Foto> fotos = new ArrayList<>();
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_FOTO_X_ID_DEP");
        jdbcCall.declareParameters(
                new SqlParameter("IN_ID_DEP", OracleTypes.NUMBER),
                new SqlOutParameter("OUT_PC_GET_FOTO_DEP", OracleTypes.CURSOR),
                new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
                new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));
        
        Map<String, Object> params=new HashMap<String, Object>();
        params.put("IN_ID_DEP", idDepartamento);
        
        Map<String, Object> result = jdbcCall.execute(params);
        List<Map<String, Object>> recordset = (List<Map<String, Object>>) result.get("OUT_PC_GET_FOTO_DEP");
    	for( Map<String, Object> obj: recordset) {
    		Foto foto = new Foto();
    		Object idFoto = obj.get("FOTO_ID");
    		foto.setId(Long.parseLong(idFoto.toString()));
    		foto.setRuta(obj.get("FOTO_RUTA").toString());
    		foto.setIdDepartamento(Long.parseLong(obj.get("DEPARTAMENTO_ID").toString()));
    		fotos.add(foto);
    	}
		
		return fotos;
	}
	
	public ResponseOracle delete(long idFoto) {
		ResponseOracle response = new ResponseOracle();
		
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_DEL_FOTO_X_ID");
        jdbcCall.declareParameters(
                new SqlParameter("IN_FOTO_ID", OracleTypes.NUMBER),
                new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
                new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));
        
        Map<String, Object> params=new HashMap<String, Object>();
        params.put("IN_FOTO_ID", idFoto);
        
        Map<String, Object> result = jdbcCall.execute(params);
        
        response.setEstado(Integer.valueOf(result.get("OUT_ESTADO").toString()));
		response.setGlosa(result.get("OUT_GLOSA").toString());
		
		return response;
	}
	
}

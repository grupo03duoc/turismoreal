package com.portafolio.turismoReal.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.portafolio.turismoReal.message.response.ResponseOracle;
import com.portafolio.turismoReal.model.Empresa;

import oracle.jdbc.OracleTypes;

@Repository
public class EmpresaRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public ResponseOracle create(Empresa empresa) {
		ResponseOracle response = new ResponseOracle();
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_CREAR_EMPRESA")
				.withoutProcedureColumnMetaDataAccess();
		jdbcCall.declareParameters(new SqlParameter("IN_EMP_NOMBRE", OracleTypes.VARCHAR),
				new SqlParameter("IN_EMP_RUT", OracleTypes.VARCHAR),
				new SqlParameter("IN_EMP_RUBRO", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IN_EMP_NOMBRE", empresa.getNombre());
		params.put("IN_EMP_RUT", empresa.getRut());
		params.put("IN_EMP_RUBRO", empresa.getRubro());

		Map<String, Object> result = jdbcCall.execute(params);

		response.setEstado(Integer.valueOf(result.get("OUT_ESTADO").toString()));
		response.setGlosa(result.get("OUT_GLOSA").toString());
		return response;
	}
	
	public ResponseOracle update(Empresa empresa) {
		ResponseOracle response = new ResponseOracle();
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_UPDATE_EMPRESA")
				.withoutProcedureColumnMetaDataAccess();
		jdbcCall.declareParameters(
				new SqlParameter("IN_EMPRESA_ID", OracleTypes.NUMBER),
				new SqlParameter("IN_EMPRESA_NOMBRE", OracleTypes.VARCHAR),
				new SqlParameter("IN_EMPRESA_RUT", OracleTypes.VARCHAR),
				new SqlParameter("IN_EMPRESA_RUBRO", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IN_EMPRESA_ID", empresa.getId());
		params.put("IN_EMPRESA_NOMBRE", empresa.getNombre());
		params.put("IN_EMPRESA_RUT", empresa.getRut());
		params.put("IN_EMPRESA_RUBRO", empresa.getRubro());

		Map<String, Object> result = jdbcCall.execute(params);

		response.setEstado(Integer.valueOf(result.get("OUT_ESTADO").toString()));
		response.setGlosa(result.get("OUT_GLOSA").toString());
		return response;
	}
	
	@SuppressWarnings("unchecked")
	public Empresa getEmpresaPorID(long id) {
		Empresa empresa = new Empresa();
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_EMPRESA_X_ID");
		jdbcCall.declareParameters(new SqlParameter("IN_EMP_ID", OracleTypes.NUMERIC),
				new SqlOutParameter("OUT_PC_GET_EMP", OracleTypes.CURSOR),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IN_EMP_ID", id);

		Map<String, Object> result = jdbcCall.execute(params);

		List<Map<String, Object>> recordset = (List<Map<String, Object>>) result.get("OUT_PC_GET_EMP");
		for (Map<String, Object> obj : recordset) {
			empresa.setId(Long.parseLong(obj.get("EMPRESA_ID").toString()));
			empresa.setNombre(obj.get("EMPRESA_NOMBRE").toString());
			empresa.setRut(obj.get("EMPRESA_RUT").toString());
			empresa.setRubro(obj.get("EMPRESA_RUBRO").toString());
		}
		return empresa;
	}
	
	@SuppressWarnings("unchecked")
	public List<Empresa> getAllEmpresa(){
		List<Empresa> empresas = new ArrayList<>();
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_EMPRESA_ALL");

		jdbcCall.declareParameters(new SqlOutParameter("OUT_PC_GET_EMP", OracleTypes.CURSOR),
				                   new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
                                   new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));
		Map<String, Object> result = jdbcCall.execute();
		List<Map<String, Object>> recordset = (List<Map<String,  Object>>) result.get("OUT_PC_GET_EMP");
		 for( Map<String,  Object> obj: recordset) {
			 Empresa empresa = new Empresa();
			 empresa.setId(Long.parseLong(obj.get("EMPRESA_ID").toString()));
			 empresa.setNombre(obj.get("EMPRESA_NOMBRE").toString());
			 empresa.setRut(obj.get("EMPRESA_RUT").toString());
			 empresa.setRubro(obj.get("EMPRESA_RUBRO").toString());     	
        	 empresas.add(empresa);
		 }
		return empresas;
	}
}

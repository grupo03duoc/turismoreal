package com.portafolio.turismoReal.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.portafolio.turismoReal.message.response.ResponseOracle;
import com.portafolio.turismoReal.model.CheckIn;

import oracle.jdbc.OracleTypes;

@Repository
public class CheckInRepository {

	
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	public ResponseOracle create(CheckIn checkin) {
		ResponseOracle response = new ResponseOracle();
		 SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_CREAR_CHECK_IN").withoutProcedureColumnMetaDataAccess();
		 jdbcCall.declareParameters(
                 new SqlParameter("IN_CHECK_IN_NOM_EMPLE", OracleTypes.VARCHAR),
                 new SqlParameter("IN_CHECK_IN_CONFORME", OracleTypes.NUMBER),
                 new SqlParameter("IN_CHECK_IN_RESERVA_ID", OracleTypes.NUMERIC),
                 new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
                 new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));
		 
		 Map<String, Object> params=new HashMap<String, Object>();
	  	 params.put("IN_CHECK_IN_NOM_EMPLE",checkin.getNombreEmpleado());
	  	 params.put("IN_CHECK_IN_CONFORME",checkin.getConforme());
	  	 params.put("IN_CHECK_IN_RESERVA_ID", checkin.getIdReserva());
	  	 
		 Map<String, Object> result = jdbcCall.execute(params);
		 response.setEstado(Integer.valueOf(result.get("OUT_ESTADO").toString()));
		 response.setGlosa(result.get("OUT_GLOSA").toString());
		
		return response;
	}
	
	@SuppressWarnings("unchecked")
	public CheckIn getCheckInPorReserva(long id) {
		CheckIn check = null;
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_CHECK_IN_X_RES_ID");
		jdbcCall.declareParameters(new SqlParameter("IN_RES_ID", OracleTypes.NUMERIC),
				new SqlOutParameter("OUT_PC_GET_RES_ID", OracleTypes.CURSOR),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IN_RES_ID", id);

		Map<String, Object> result = jdbcCall.execute(params);

		List<Map<String, Object>> recordset = (List<Map<String, Object>>) result.get("OUT_PC_GET_RES_ID");
		for (Map<String, Object> obj : recordset) {
			if(obj.get("CHECK_IN_ID") != null) {
				check =  new CheckIn();
				check.setId(Long.parseLong(obj.get("CHECK_IN_ID").toString()));
				check.setNombreEmpleado(obj.get("CHECK_IN_NOMBRE_EMPLEADO").toString());
				check.setConforme(Integer.valueOf(obj.get("CHECK_IN_CONFORME").toString()));
				check.setIdReserva(Long.parseLong(obj.get("RESERVA_ID").toString()));
			}
		}
		
		return check;	
	}
}

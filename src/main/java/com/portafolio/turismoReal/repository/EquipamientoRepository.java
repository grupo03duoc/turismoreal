package com.portafolio.turismoReal.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.portafolio.turismoReal.message.response.ResponseOracle;
import com.portafolio.turismoReal.model.Equipamiento;

import oracle.jdbc.OracleTypes;

@Repository
public class EquipamientoRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public ResponseOracle create(Equipamiento equipo) {
		ResponseOracle response = new ResponseOracle();

		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_INSERT_EQUIP")
				.withoutProcedureColumnMetaDataAccess();
		jdbcCall.declareParameters(new SqlParameter("IN_EQUIP_NOMBRE", OracleTypes.VARCHAR),
				new SqlParameter("IN_EQUIP_CANT_INVENT", OracleTypes.NUMBER),
				new SqlParameter("IN_EQUIP_VALOR_UN", OracleTypes.NUMBER),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IN_EQUIP_NOMBRE", equipo.getNombre());
		params.put("IN_EQUIP_CANT_INVENT", equipo.getInventario());
		params.put("IN_EQUIP_VALOR_UN", equipo.getValorUnitario());

		Map<String, Object> result = jdbcCall.execute(params);

		response.setEstado(Integer.valueOf(result.get("OUT_ESTADO").toString()));
		response.setGlosa(result.get("OUT_GLOSA").toString());

		return response;
	}

	@SuppressWarnings("unchecked")
	public List<Equipamiento> allEquipamientoByDep(Long idDepa) {
		List<Equipamiento> equipos = new ArrayList<>();
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_EQUIP_X_ID_DEP");
		jdbcCall.declareParameters(new SqlParameter("IN_ID_DEP", OracleTypes.NUMBER),
				new SqlOutParameter("OUT_PC_GET_EQUIP_DEP", OracleTypes.CURSOR),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IN_ID_DEP", idDepa);

		Map<String, Object> result = jdbcCall.execute(params);
		List<Map<String, Object>> recordset = (List<Map<String, Object>>) result.get("OUT_PC_GET_EQUIP_DEP");
		for (Map<String, Object> obj : recordset) {
			Equipamiento equipo = new Equipamiento();
			equipo.setId(Long.parseLong(obj.get("EQUIPAMIENTO_ID").toString()));
			equipo.setNombre(obj.get("EQUIPAMIENTO_NOMBRE").toString());
			equipo.setInventario(Integer.valueOf(obj.get("EQUIPAMIENTO_CANT_INVENTARIO").toString()));
			equipo.setValorUnitario(Integer.valueOf(obj.get("EQUIPAMIENTO_VALOR_UNITARIO").toString()));
			equipos.add(equipo);
		}

		return equipos;

	}

	@SuppressWarnings("unchecked")
	public List<Equipamiento> allEquipamiento() {
		List<Equipamiento> equipos = new ArrayList<>();
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_EQUIPAMIENTO");
		jdbcCall.declareParameters(new SqlOutParameter("OUT_PC_GET_EQUIP", OracleTypes.CURSOR),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> result = jdbcCall.execute();
		List<Map<String, Object>> recordset = (List<Map<String, Object>>) result.get("OUT_PC_GET_EQUIP");
		for (Map<String, Object> obj : recordset) {
			Equipamiento equipo = new Equipamiento();
			Object idObj = obj.get("EQUIPAMIENTO_ID");
			equipo.setId(Long.parseLong(idObj.toString()));
			equipo.setNombre(obj.get("EQUIPAMIENTO_NOMBRE").toString());
			equipo.setInventario(Integer.valueOf(obj.get("EQUIPAMIENTO_CANT_INVENTARIO").toString()));
			equipo.setValorUnitario(Integer.valueOf(obj.get("EQUIPAMIENTO_VALOR_UNITARIO").toString()));
			equipos.add(equipo);
		}
		return equipos;

	}

	public ResponseOracle delete(long id) {
		ResponseOracle response = new ResponseOracle();
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_DEL_EQUIPAMIENTO");
		jdbcCall.declareParameters(new SqlParameter("IN_EQUIP_ID", OracleTypes.NUMBER),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IN_EQUIP_ID", id);
		Map<String, Object> result = jdbcCall.execute(params);

		response.setEstado(Integer.valueOf(result.get("OUT_ESTADO").toString()));
		response.setGlosa(result.get("OUT_GLOSA").toString());

		return response;
	}
}

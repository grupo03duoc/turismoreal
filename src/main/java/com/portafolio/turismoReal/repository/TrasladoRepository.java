package com.portafolio.turismoReal.repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.portafolio.turismoReal.message.request.TrasladoForm;
import com.portafolio.turismoReal.message.response.ResponseOracle;

import oracle.jdbc.OracleTypes;

@Repository
public class TrasladoRepository {

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	public ResponseOracle create(TrasladoForm traslado) {
		ResponseOracle response = new ResponseOracle();
		Date fecha = new Date();
		try {
			fecha = new SimpleDateFormat("dd-MM-yyyy").parse(traslado.getFecha());
		} catch (ParseException e) {

			e.printStackTrace();
		}
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_CREAR_TRASLADO");
		jdbcCall.declareParameters(new SqlParameter("IN_TRASLADO_FECHA", OracleTypes.DATE),
				new SqlParameter("IN_TRASLADO_UBICACION", OracleTypes.VARCHAR),
				new SqlParameter("IN_TRASLADO_CHOFER_ID", OracleTypes.NUMBER),
				new SqlParameter("IN_TRASLADO_RESERV_ID", OracleTypes.NUMBER),
				new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
				new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IN_TRASLADO_FECHA", fecha);
		params.put("IN_TRASLADO_UBICACION", traslado.getUbicacion());
		params.put("IN_TRASLADO_CHOFER_ID", traslado.getChofer());
		params.put("IN_TRASLADO_RESERV_ID", traslado.getReserva());

		Map<String, Object> result = jdbcCall.execute(params);

		response.setEstado(Integer.valueOf(result.get("OUT_ESTADO").toString()));
		response.setGlosa(result.get("OUT_GLOSA").toString());
		return response;
	}
}

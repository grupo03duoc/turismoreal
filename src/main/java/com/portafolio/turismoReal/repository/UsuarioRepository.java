package com.portafolio.turismoReal.repository;


import java.util.*;

import org.springframework.stereotype.Repository;

import com.portafolio.turismoReal.message.response.ResponseOracle;
import com.portafolio.turismoReal.model.Perfil;
import com.portafolio.turismoReal.model.Usuario;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

@Repository
public class UsuarioRepository {
    
    @Autowired
    private JdbcTemplate jdbcTemplate;
    

    @SuppressWarnings("unchecked")
    public List<Usuario> AllUsuarios() {
       List<Usuario> usuarios = new ArrayList<>();
       SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_USUARIOS_PERFIL");

		jdbcCall.declareParameters(new SqlOutParameter("OUT_PC_GET_USUARIOS", OracleTypes.CURSOR));

		Map<String, Object> result = jdbcCall.execute();
		List<Map<String, Object>> recordset = (List<Map<String, Object>>) result.get("OUT_PC_GET_USU_PERF");
         for( Map<String, Object> obj: recordset) {
        	 Usuario user = new Usuario();
        	 Set<Perfil> perfiles = new HashSet<>();
        	 Object idObj = obj.get("USUARIO_ID");
        	 user.setId(Long.parseLong(idObj.toString()));
        	 user.setUsername(obj.get("USUARIO_NOMBRE").toString());
        	 user.setCorreo(obj.get("USUARIO_CORREO").toString());
        	 user.setNombre(obj.get("NOMBRE").toString());
        	 Object estadoObj = obj.get("USUARIO_ESTADO");
        	 String estado = estadoObj.toString();
        	 user.setEstadoUsuario(Integer.valueOf(estado));
        	 Perfil perfil = new Perfil();
        	 Object perfilObj = obj.get("PERFIL_ID");
        	 perfil.setId(Long.parseLong(perfilObj.toString()));
        	 perfil.setNombre(obj.get("PERFIL_DESCRIPCION").toString());
        	 perfiles.add(perfil);
        	 user.setPerfil(perfiles);
        	 usuarios.add(user);
        	
         }
       return usuarios;
    }
    
    @SuppressWarnings("unchecked")
    public Usuario findbyid(Long id) {
    	Usuario user = new Usuario();
    	SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_USUARIOS_X_ID");
    	jdbcCall.declareParameters(new SqlParameter("IN_ID_USUARIO", OracleTypes.NUMERIC),
    			                  new SqlOutParameter("OUT_PC_GET_USU_ID", OracleTypes.CURSOR),
    			                  new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
                                  new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));
    	 Map<String, Object> params=new HashMap<String, Object>();
    	 params.put("IN_ID_USUARIO", id);
    	 Map<String, Object> result = jdbcCall.execute(params);
    	 List<Map<String, Object>> recordset = (List<Map<String, Object>>) result.get("OUT_PC_GET_USU_ID");
    	 for( Map<String, Object> obj: recordset) {
        	 Set<Perfil> perfiles = new HashSet<>();
        	 user.setId(id);
        	 user.setUsername(obj.get("USUARIO_NOMBRE").toString());
        	 user.setCorreo(obj.get("USUARIO_CORREO").toString());
        	 user.setNombre(obj.get("NOMBRE").toString());
        	 Object estadoObj = obj.get("USUARIO_ESTADO");
        	 String estado = estadoObj.toString();
        	 user.setEstadoUsuario(Integer.valueOf(estado));
        	 Perfil perfil = new Perfil();
        	 Object perfilObj = obj.get("PERFIL_ID");
        	 perfil.setId(Long.parseLong(perfilObj.toString()));
        	 perfil.setNombre(obj.get("PERFIL_DESCRIPCION").toString());
        	 perfiles.add(perfil);
        	 user.setPerfil(perfiles);
    	 }
    	 
    	return user;
    }
    
    @SuppressWarnings("unchecked")
    public Usuario findbyUsername(String username) {
    	Usuario user = new Usuario();
    	SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_USUARIOS_X_NOMBRE");
    	jdbcCall.declareParameters(new SqlParameter("IN_NOM_USUARIO", OracleTypes.VARCHAR),
    			                  new SqlOutParameter("OUT_PC_GET_USU_NOM", OracleTypes.CURSOR),
    			                  new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
                                  new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));
    	 Map<String, Object> params=new HashMap<String, Object>();
    	 params.put("IN_NOM_USUARIO", username);
    	 Map<String, Object> result = jdbcCall.execute(params);
    	 List<Map<String, Object>> recordset = (List<Map<String, Object>>) result.get("OUT_PC_GET_USU_NOM");
    	 for( Map<String, Object> obj: recordset) {
        	 Set<Perfil> perfiles = new HashSet<>();
        	 user.setId(Long.parseLong(obj.get("USUARIO_ID").toString()));
        	 user.setUsername(obj.get("USUARIO_NOMBRE").toString());
        	 user.setPassword(obj.get("USUARIO_CLAVE").toString());
        	 user.setCorreo(obj.get("USUARIO_CORREO").toString());
        	 user.setNombre(obj.get("NOMBRE").toString());
        	 user.setEstadoUsuario(Integer.valueOf(obj.get("USUARIO_ESTADO").toString()));
        	 Perfil perfil = new Perfil();
        	 perfil.setId(Long.parseLong(obj.get("PERFIL_ID").toString()));
        	 perfil.setNombre(obj.get("PERFIL_DESCRIPCION").toString());
        	 perfiles.add(perfil);
        	 user.setPerfil(perfiles);
    	 }
    	 
    	return user;
    }
    
    
    public ResponseOracle update(Usuario user) {
    	ResponseOracle response = new ResponseOracle();
    	
    	SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_UPDATE_USUARIO_PERFIL").withoutProcedureColumnMetaDataAccess();
    	jdbcCall.declareParameters(new SqlParameter("IN_USUARIO_ID", OracleTypes.NUMERIC),
    		                       new SqlParameter("IN_NOMBRE", OracleTypes.VARCHAR),
    			                   new SqlParameter("IN_USUARIO_NOMBRE", OracleTypes.VARCHAR),
    			                   new SqlParameter("IN_USUARIO_CORREO", OracleTypes.VARCHAR),
    			                   new SqlParameter("IN_USUARIO_ESTADO", OracleTypes.NUMBER),
    			                   new SqlParameter("IN_USUARIO_CLAVE", OracleTypes.VARCHAR),
    			                   new SqlParameter("IN_PERFIL_ID", OracleTypes.NUMERIC),
    			                   new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
    			                   new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));
    	
    	  Map<String, Object> params=new HashMap<String, Object>();
    	  params.put("IN_USUARIO_ID", user.getId());
    	  params.put("IN_NOMBRE",user.getNombre());
    	  params.put("IN_USUARIO_NOMBRE",user.getUsername());
    	  params.put("IN_USUARIO_CORREO", user.getCorreo());
    	  params.put("IN_USUARIO_ESTADO", user.getEstadoUsuario());
    	  params.put("IN_USUARIO_CLAVE", user.getPassword());
    	  for(Perfil perfil : user.getPerfil()) {
    		  params.put("IN_PERFIL_ID", perfil.getId());
    	  }
    	  
    	
    	Map<String, Object> result = jdbcCall.execute(params);
    	response.setEstado(Integer.valueOf(result.get("OUT_ESTADO").toString()));
    	response.setGlosa(result.get("OUT_GLOSA").toString());
    	return response;	
    }
    
    public ResponseOracle create(Usuario user) {
    	ResponseOracle response = new ResponseOracle();
    	SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_INSERT_USU_PERFIL").withoutProcedureColumnMetaDataAccess();
    	jdbcCall.declareParameters(
    			                   new SqlParameter("IN_USUARIO_NOMBRE", OracleTypes.VARCHAR),
    			                   new SqlParameter("IN_USUARIO_CORREO", OracleTypes.VARCHAR),
    			                   new SqlParameter("IN_USUARIO_ESTADO", OracleTypes.NUMBER),
    			                   new SqlParameter("IN_USUARIO_CLAVE", OracleTypes.VARCHAR),
    			                   new SqlParameter("IN_NOMBRE", OracleTypes.VARCHAR),
    			                   new SqlParameter("IN_USUARIO_PERFIL_ID", OracleTypes.NUMBER),
    			                   new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
    			                   new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));
    	
      Map<String, Object> params=new HashMap<String, Object>();
  	  params.put("IN_NOMBRE",user.getNombre());
  	  params.put("IN_USUARIO_NOMBRE",user.getUsername());
  	  params.put("IN_USUARIO_CORREO", user.getCorreo());
  	  params.put("IN_USUARIO_ESTADO", user.getEstadoUsuario());
  	  params.put("IN_USUARIO_CLAVE", user.getPassword());
  	  for(Perfil perfil : user.getPerfil()) {
		  params.put("IN_USUARIO_PERFIL_ID", perfil.getId());
	  }
  	  Map<String, Object> result = jdbcCall.execute(params);
	  response.setEstado(Integer.valueOf(result.get("OUT_ESTADO").toString()));
	  response.setGlosa(result.get("OUT_GLOSA").toString());
	
      return response;
    }

    public ResponseOracle disable(int idUsuario,int estado) {
    	ResponseOracle response = new ResponseOracle();
    	SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_DESAC_USU").withoutProcedureColumnMetaDataAccess();
    	jdbcCall.declareParameters(
    			                   new SqlParameter("IN_ID_USUARIO", OracleTypes.NUMBER),
    			                   new SqlParameter("IN_ESTADO_USU", OracleTypes.NUMBER),
    			                   new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
    			                   new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));
    	
    	Map<String, Object> params=new HashMap<String, Object>();
  	    params.put("IN_ID_USUARIO", idUsuario);
  	    params.put("IN_ESTADO_USU",estado);
    	Map<String, Object> result = jdbcCall.execute(params);
    	response.setEstado(Integer.valueOf(result.get("OUT_ESTADO").toString()));
    	response.setGlosa(result.get("OUT_GLOSA").toString());
    	
    	return response;
    }
   
	
}

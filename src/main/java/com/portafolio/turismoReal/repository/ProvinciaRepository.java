package com.portafolio.turismoReal.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.portafolio.turismoReal.model.Provincia;

import oracle.jdbc.OracleTypes;

@Repository
public class ProvinciaRepository {

	
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@SuppressWarnings("unchecked")
	public List<Provincia> AllProvincias(Long idRegion){
		 List<Provincia> provincias = new ArrayList<>();
	       SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("SP_GET_PROV_REG_ID");

			jdbcCall.declareParameters(new SqlParameter("IN_REG_ID", OracleTypes.NUMERIC),
					                   new SqlOutParameter("OUT_PC_GET_PROV", OracleTypes.CURSOR),
					                   new SqlOutParameter("OUT_GLOSA", OracleTypes.VARCHAR),
                                       new SqlOutParameter("OUT_ESTADO", OracleTypes.NUMBER));
			  Map<String, Object> params=new HashMap<String, Object>();
	    	  params.put("IN_REG_ID", idRegion);
	    	  
			Map<String, Object> result = jdbcCall.execute(params);
			List<Map<String, Object>> recordset = (List<Map<String, Object>>) result.get("OUT_PC_GET_PROV");
			 for( Map<String, Object> obj: recordset) {
				 Provincia provincia = new Provincia();
	        	 provincia.setId(Long.parseLong(obj.get("PROVINCIA_ID").toString()));
	        	 provincia.setNombre(obj.get("PROVINCIA_NOMBRE").toString());
	        	 provincia.setRegion(Long.parseLong(obj.get("REGION_ID").toString()));
	        	 provincias.add(provincia);
			 }
 
			return provincias;
	}
}

package com.portafolio.turismoReal;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class TurismoRealApplication {
    

	public static void main(String[] args) {
		SpringApplication.run(TurismoRealApplication.class, args);
	}

}

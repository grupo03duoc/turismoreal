package com.portafolio.turismoReal.model;


public class Pago {

	private Long id;
	private String fecha;
	private int monto;
	private TipoPago tipoPago;
	private Long idRserva;
	
	public Pago() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public int getMonto() {
		return monto;
	}

	public void setMonto(int monto) {
		this.monto = monto;
	}

	public TipoPago getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(TipoPago tipoPago) {
		this.tipoPago = tipoPago;
	}

	public Long getIdRserva() {
		return idRserva;
	}

	public void setIdRserva(Long idRserva) {
		this.idRserva = idRserva;
	}

	
	
	
	
}

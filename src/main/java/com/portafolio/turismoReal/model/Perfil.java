package com.portafolio.turismoReal.model;

public class Perfil {

	private Long id;
	private String nombre;
	
	public Perfil() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}

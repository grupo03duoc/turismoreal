package com.portafolio.turismoReal.model;

import java.util.HashSet;
import java.util.Set;

public class Usuario {
   
    private Long id;
    
    private String nombre;
    
    private String username;

    private String correo;

    private String password;
    
    private int estadoUsuario;
    
    private Set<Perfil> perfil = new HashSet<>();
    
    public Usuario() {}

    public Usuario(String nombre, String username, String correo, String password,int estadoUsuario) {
        this.nombre = nombre;
        this.username = username;
        this.correo = correo;
        this.estadoUsuario=estadoUsuario;
        this.password = password;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

        public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
        
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}
	

	public int getEstadoUsuario() {
		return estadoUsuario;
	}

	public void setEstadoUsuario(int estadoUsuario) {
		this.estadoUsuario = estadoUsuario;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Perfil> getPerfil() {
		return perfil;
	}

	public void setPerfil(Set<Perfil> perfil) {
		this.perfil = perfil;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", nombre=" + nombre + ", username=" + username + ", correo=" + correo
				+ ", password=" + password + ", estadoUsuario=" + estadoUsuario + ", perfil=" + perfil + "]";
	}




	
	
	
	
    
}

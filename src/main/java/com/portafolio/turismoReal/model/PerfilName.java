package com.portafolio.turismoReal.model;

public enum PerfilName {

	ROLE_USER,
	ROLE_ADMIN,
}

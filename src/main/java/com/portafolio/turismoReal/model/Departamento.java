package com.portafolio.turismoReal.model;

import java.util.List;

public class Departamento {

	private Long id;
	private String direccion;
	private String caracteristicas;
	private int valorNoche;
	private int dormitorios;
	private int camas;
	private Comuna comuna;
	private List<Foto> fotos;
	private List<Equipamiento> equipamientos;
	
	public Departamento() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCaracteristicas() {
		return caracteristicas;
	}

	public void setCaracteristicas(String caracteristicas) {
		this.caracteristicas = caracteristicas;
	}

	public int getValorNoche() {
		return valorNoche;
	}

	public void setValorNoche(int valorNoche) {
		this.valorNoche = valorNoche;
	}

	public int getDormitorios() {
		return dormitorios;
	}

	public void setDormitorios(int dormitorios) {
		this.dormitorios = dormitorios;
	}

	public int getCamas() {
		return camas;
	}

	public void setCamas(int camas) {
		this.camas = camas;
	}

	public Comuna getComuna() {
		return comuna;
	}

	public void setComuna(Comuna comuna) {
		this.comuna = comuna;
	}

	public List<Foto> getFotos() {
		return fotos;
	}

	public void setFotos(List<Foto> fotos) {
		this.fotos = fotos;
	}

	public List<Equipamiento> getEquipamientos() {
		return equipamientos;
	}

	public void setEquipamientos(List<Equipamiento> equipamientos) {
		this.equipamientos = equipamientos;
	}

	@Override
	public String toString() {
		return "Departamento [id=" + id + ", direccion=" + direccion + ", caracteristicas=" + caracteristicas
				+ ", valorNoche=" + valorNoche + ", dormitorios=" + dormitorios + ", camas=" + camas + ", comuna="
				+ comuna + "]";
	}

	
	
	
	
}

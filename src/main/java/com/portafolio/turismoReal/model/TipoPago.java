package com.portafolio.turismoReal.model;

public class TipoPago {

	private Long id;
	private String descripcion;
	
	public TipoPago() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}

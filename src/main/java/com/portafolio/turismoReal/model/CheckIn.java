package com.portafolio.turismoReal.model;

public class CheckIn {

	private Long id;
	private String nombreEmpleado;
	private int conforme;
	private Long idReserva;
	
	public CheckIn() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombreEmpleado() {
		return nombreEmpleado;
	}

	public void setNombreEmpleado(String nombreEmpleado) {
		this.nombreEmpleado = nombreEmpleado;
	}

	public int getConforme() {
		return conforme;
	}

	public void setConforme(int conforme) {
		this.conforme = conforme;
	}

	public Long getIdReserva() {
		return idReserva;
	}

	public void setIdReserva(Long idReserva) {
		this.idReserva = idReserva;
	}
	
	
}

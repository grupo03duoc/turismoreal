package com.portafolio.turismoReal.services;

import java.io.ByteArrayInputStream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.portafolio.turismoReal.model.Departamento;
import com.portafolio.turismoReal.model.Region;
import com.portafolio.turismoReal.model.Reserva;
import com.portafolio.turismoReal.model.Tour;
import com.portafolio.turismoReal.repository.DepartamentoRepository;
import com.portafolio.turismoReal.repository.RegionRepository;
import com.portafolio.turismoReal.repository.ReservaRepository;
import com.portafolio.turismoReal.repository.TourRepository;
@Service
public class GenerarFileService {

	@Autowired
    ReservaRepository reservaRepository;
	
	@Autowired
    DepartamentoRepository departamentoRepository;
	
	@Autowired
    RegionRepository regionRepository;
	
	@Autowired
    TourRepository tourRepository;
	
	public ByteArrayInputStream GenerarInforme(String fechaInicio,String fechaTermino, long idRegion) throws IOException {
		try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			Sheet sheet = workbook.createSheet("Reporte TurismoReal");
			CellStyle styleCabecera = crearBordes(workbook);
			styleCabecera.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			styleCabecera.setAlignment(HorizontalAlignment.CENTER);
			
			CellStyle styleData = crearBordes(workbook);
			
			CellStyle styleBonos = crearBordes(workbook);
			styleBonos.setDataFormat((short) 5);
			styleBonos.setAlignment(HorizontalAlignment.CENTER);
			
			Font font = workbook.createFont();
			font.setBold(true);
			font.setColor(IndexedColors.WHITE.getIndex());
			styleCabecera.setFont(font);
			  Row header = sheet.createRow(0);
			  Cell cellfechaInicio = header.createCell(0);
			  cellfechaInicio.setCellValue("Fecha Inicio");
			  cellfechaInicio.setCellStyle(styleCabecera);
			  sheet.autoSizeColumn(0);
			  Cell cellfechaTermino = header.createCell(1);
			  cellfechaTermino.setCellValue("Fecha Termino");
			  cellfechaTermino.setCellStyle(styleCabecera);
			  sheet.autoSizeColumn(1);
			  Cell cellCantidadPersona = header.createCell(2);
			  cellCantidadPersona.setCellValue("Cantidad de Personas");
			  cellCantidadPersona.setCellStyle(styleCabecera);
			  sheet.autoSizeColumn(2);
              Cell cellTotal =  header.createCell(3);
              cellTotal.setCellValue("Total Reserva");
              cellTotal.setCellStyle(styleCabecera);
              sheet.autoSizeColumn(3);
              Cell cellDireccion = header.createCell(4);
              cellDireccion.setCellValue("Dirección Departamento");
              cellDireccion.setCellStyle(styleCabecera);
              sheet.autoSizeColumn(4);
              Cell cellComuna = header.createCell(5);
              cellComuna.setCellValue("Comuna");
              cellComuna.setCellStyle(styleCabecera);
              sheet.autoSizeColumn(5);
              Cell cellRegion = header.createCell(6);
              cellRegion.setCellValue("Región");
              cellRegion.setCellStyle(styleCabecera);
              sheet.autoSizeColumn(6);
              Cell cellCaracteristicas = header.createCell(7);
              cellCaracteristicas.setCellValue("Caracteristicas");
              cellCaracteristicas.setCellStyle(styleCabecera);
              sheet.autoSizeColumn(7);
              Cell cellValorNoche = header.createCell(8);
              cellValorNoche.setCellValue("Valor Noche");
              cellValorNoche.setCellStyle(styleCabecera);
              sheet.autoSizeColumn(8);
              Cell cellDormitorio = header.createCell(9);
              cellDormitorio.setCellValue("Dormitorio");
              cellDormitorio.setCellStyle(styleCabecera);
              sheet.autoSizeColumn(9);
              Cell cellCamas = header.createCell(10);
              cellCamas.setCellValue("Camas");
              cellCamas.setCellStyle(styleCabecera);
              sheet.autoSizeColumn(10);
              Cell cellTour = header.createCell(11);
              cellTour.setCellValue("Tour");
              cellTour.setCellStyle(styleCabecera);
              sheet.autoSizeColumn(11);
              Cell cellDescripcion = header.createCell(12);
              cellDescripcion.setCellValue("Descripción");
              cellDescripcion.setCellStyle(styleCabecera);
              sheet.autoSizeColumn(12);
              Cell cellValorPersona = header.createCell(13);
              cellValorPersona.setCellValue("Valor por persona");
              cellValorPersona.setCellStyle(styleCabecera);
              sheet.autoSizeColumn(13);
			  
			  int rowIndex = 1;
			  int columna = 1;
			Region region = regionRepository.GetRegion(idRegion);
			List<Reserva> reservas = reservaRepository.getAllReservaPorPeriodo(fechaInicio, fechaTermino,idRegion);
			for(Reserva reserva : reservas) {
				Departamento depa = departamentoRepository.getDepartamentoById(reserva.getDepartamento());
				Tour tour = tourRepository.getTourPorReserva(reserva.getId());
				Row row = sheet.createRow(rowIndex++);
				columna = 0;
				Cell rowFechaInicio = row.createCell(columna++);
				rowFechaInicio.setCellValue(reserva.getFechaInicio());
				rowFechaInicio.setCellStyle(styleData);

				Cell rowFechaTermino = row.createCell(columna++);
				rowFechaTermino.setCellValue(reserva.getFechaTermino());
				rowFechaTermino.setCellStyle(styleData);

				Cell rowPersonas = row.createCell(columna++);
				rowPersonas.setCellValue(reserva.getPersonas());
				rowPersonas.setCellStyle(styleData);

				Cell rowTotal = row.createCell(columna++);
				rowTotal.setCellValue(reserva.getTotal());
				rowTotal.setCellStyle(styleData);
                
				Cell rowDireccion = row.createCell(columna++);
				rowDireccion.setCellValue(depa.getDireccion());
				rowDireccion.setCellStyle(styleData);
				
				Cell rowComuna = row.createCell(columna++);
				rowComuna.setCellValue(depa.getDireccion());
				rowComuna.setCellStyle(styleData);

				Cell rowRegion = row.createCell(columna++);
				rowRegion.setCellValue(region.getNombre());
				rowRegion.setCellStyle(styleData);
				
				Cell rowCaracteristica = row.createCell(columna++);
				rowCaracteristica.setCellValue(depa.getCaracteristicas());
				rowCaracteristica.setCellStyle(styleData);
				
				Cell rowValorNoche = row.createCell(columna++);
				rowValorNoche.setCellValue(depa.getValorNoche());
				rowValorNoche.setCellStyle(styleData);
				
				Cell rowDormitorio = row.createCell(columna++);
				rowDormitorio.setCellValue(depa.getDormitorios());
				rowDormitorio.setCellStyle(styleData);
				
				Cell rowCamas = row.createCell(columna++);
				rowCamas.setCellValue(depa.getCamas());
				rowCamas.setCellStyle(styleData);
				
				Cell rowNombre = row.createCell(columna++);
				rowNombre.setCellValue(tour.getNombre());
				rowNombre.setCellStyle(styleData);

				Cell rowDescripcion = row.createCell(columna++);
				rowDescripcion.setCellValue(tour.getDescripcion());
				rowDescripcion.setCellStyle(styleData);
				
				Cell rowValorPersona = row.createCell(columna++);
				rowValorPersona.setCellValue(tour.getValor_persona());
				rowValorPersona.setCellStyle(styleData);
			}
			
			workbook.write(out);
			return new ByteArrayInputStream(out.toByteArray());
		}
		
	}
	
	public static CellStyle crearBordes(Workbook wb) {
		
			CellStyle cellStyle = wb.createCellStyle();
			cellStyle.setBorderTop(BorderStyle.THIN);
			cellStyle.setBorderBottom(BorderStyle.THIN);
			cellStyle.setBorderLeft(BorderStyle.THIN);
			cellStyle.setBorderRight(BorderStyle.THIN);
			return cellStyle;
    }
}

package com.portafolio.turismoReal.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.portafolio.turismoReal.message.request.ReservaTourForm;
import com.portafolio.turismoReal.message.request.TourForm;
import com.portafolio.turismoReal.message.response.ResponseOracle;
import com.portafolio.turismoReal.model.Tour;
import com.portafolio.turismoReal.repository.TourRepository;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/tour")
public class TourController {

	@Autowired
    TourRepository tourRepository;
	
	@PostMapping("/create")
	public ResponseEntity<?> createTour(@Valid @RequestBody TourForm tour) {
		 
		 ResponseOracle response = tourRepository.create(tour);
		 
		 return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PostMapping("/create/reservaTour")
	public ResponseEntity<?> createReservaTour(@Valid @RequestBody ReservaTourForm tour) {
		 
		 ResponseOracle response = tourRepository.createReservaTour(tour);
		 
		 return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	 @PutMapping("/update")
	 public ResponseEntity<?> updateTour(@Valid @RequestBody TourForm tour) {
		 ResponseOracle response = tourRepository.update(tour);
		 
		 return new ResponseEntity<>(response, HttpStatus.OK);
	 }
	 
	 @GetMapping("/get_por_region/{id}")
	 public List<Tour> getTourPorRegion(@PathVariable long id){
		 return tourRepository.getAllTourPorRegion(id);
	 }
}

package com.portafolio.turismoReal.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.portafolio.turismoReal.message.request.ChoferForm;
import com.portafolio.turismoReal.message.response.ResponseOracle;
import com.portafolio.turismoReal.model.Chofer;
import com.portafolio.turismoReal.repository.ChoferRepository;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/chofer")
public class ChoferController {

	@Autowired
    ChoferRepository choferRepository;
	
	@PostMapping("/create")
	public ResponseEntity<?> createChofer(@Valid @RequestBody ChoferForm chofer) {
		 
		 ResponseOracle response = choferRepository.create(chofer);
		 
		 return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	 @PutMapping("/update")
	 public ResponseEntity<?> updateChofer(@Valid @RequestBody ChoferForm chofer) {
		 ResponseOracle response = choferRepository.update(chofer);
		 
		 return new ResponseEntity<>(response, HttpStatus.OK);
	 }
	 
	 @GetMapping("/get_por_empresa/{id}")
	 public List<Chofer> getChoferPorEmpresa(@PathVariable long id){
		 return choferRepository.getChoferPorEmpresa(id);
	 }
}

package com.portafolio.turismoReal.controller;

import com.portafolio.turismoReal.message.request.RegistroForm;
import com.portafolio.turismoReal.message.response.ResponseOracle;
import com.portafolio.turismoReal.model.Perfil;
import com.portafolio.turismoReal.model.PerfilName;
import com.portafolio.turismoReal.model.Usuario;
import com.portafolio.turismoReal.repository.UsuarioRepository;
import com.portafolio.turismoReal.security.jwt.JwtProvider;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/usuario")
public class UsuarioController {
    
    @Autowired
    AuthenticationManager authenticationManager;

   

    @Autowired
    UsuarioRepository usuarioRepository;


    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtProvider jwtProvider;
    
    @PostMapping("/create")
	public ResponseEntity<?> registerUser(@Valid @RequestBody RegistroForm request) {
		try {

			// Creando Usuario
			
             Usuario user = new Usuario();
             Set<Perfil> perfiles = new HashSet<>();
             Perfil perfil = new Perfil();
             
             user.setNombre(request.getNombre());
             user.setUsername(request.getUsername());
             user.setCorreo(request.getCorreo());
             user.setEstadoUsuario(request.getHabilitado());
             user.setPassword(encoder.encode(request.getPassword()));
             
             if(request.getPerfil().equalsIgnoreCase("Administrador")) {
            	 perfil.setId(new Long(1));
             }else {
            	 perfil.setId(new Long(2));
             }
             perfiles.add(perfil);
             
             user.setPerfil(perfiles);
			
			 ResponseOracle response = usuarioRepository.create(user);

			return new ResponseEntity<>(response, HttpStatus.OK);

		}catch (Exception ex) {

			Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE,"Error en clase UsuarioController, metodo registerUser, Descripcion del error: " + ex.toString());


		}
		throw new Error("Error en el Metodo registerUser, revisar Logger");
	}

	@GetMapping("/users")
	public List<Usuario> getAllUsuarios() {
	
         
	  return  usuarioRepository.AllUsuarios();
		
	}

	@GetMapping("/getUsuario/{username}")
	public Usuario getUsuarioName(@PathVariable String username) {
		
	  return usuarioRepository.findbyUsername(username);
	}
	


	// Actualizacion de usuario
	@PutMapping("/update")
	public ResponseEntity<?> updateUsuario(@RequestBody RegistroForm usuario) {
		try {
			Usuario user = usuarioRepository.findbyid(usuario.getId());
			
			
				user.setNombre(usuario.getNombre());
				user.setUsername(usuario.getUsername());
				user.setCorreo(usuario.getCorreo());
				if(usuario.getPassword().trim().length()> 1 && !usuario.getPassword().equalsIgnoreCase(" ")) {
					user.setPassword(encoder.encode(usuario.getPassword()));
				}
				String perfil = usuario.getPerfil();
				Set<Perfil> perfiles = new HashSet<>();
				Perfil userPerfil = new Perfil();
				if(perfil.equalsIgnoreCase("Administrador")) {
					userPerfil.setNombre(PerfilName.ROLE_ADMIN.toString());	
					userPerfil.setId((long) 1);
					
				}else {
					userPerfil.setNombre(PerfilName.ROLE_USER.toString());	
					userPerfil.setId((long) 2);
				}
					perfiles.add(userPerfil);

				user.setPerfil(perfiles);
				ResponseOracle  response = usuarioRepository.update(user);
				return new ResponseEntity<>(response, HttpStatus.OK);
			
			
		} catch (Exception ex) {

			Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE,
					"Error en clase UsuarioController, metodo updateUsuario, Descripcion del error: " + ex.toString());

		}
		throw new Error("Error en el Metodo updateUsuario, revisar Logger");
	}
	
	@Transactional
	@GetMapping("/disable/{idUsuario}/{estado}")
	public ResponseEntity<?> disableUsuario(@PathVariable int idUsuario,@PathVariable int estado) {
		 ResponseOracle response = usuarioRepository.disable(idUsuario, estado);
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}

package com.portafolio.turismoReal.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.portafolio.turismoReal.message.response.ResponseOracle;
import com.portafolio.turismoReal.model.Empresa;
import com.portafolio.turismoReal.repository.EmpresaRepository;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/empresa")
public class EmpresaController {

	@Autowired
    EmpresaRepository empresaRepository;
	
	 @PostMapping("/create")
	 public ResponseEntity<?> createEmpresa(@Valid @RequestBody Empresa empresa) {
		 
		    ResponseOracle response = empresaRepository.create(empresa);
		 
			return new ResponseEntity<>(response, HttpStatus.OK);
	 }
	 
	 @GetMapping("/get/{id}")
	 public Empresa getEmpresa(@PathVariable long id) {
			
		return empresaRepository.getEmpresaPorID(id);
	 }
	 
	 @PutMapping("/update")
	 public ResponseEntity<?> updateEmpresa(@Valid @RequestBody Empresa empresa) {
		 ResponseOracle response = empresaRepository.update(empresa);
		 
		 return new ResponseEntity<>(response, HttpStatus.OK);
	 }
	 
	 @GetMapping("/getAll")
	 public List<Empresa> getAllEmpresa() {
			
		return empresaRepository.getAllEmpresa();
	 }
}

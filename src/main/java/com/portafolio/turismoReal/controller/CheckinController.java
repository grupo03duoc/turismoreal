package com.portafolio.turismoReal.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.portafolio.turismoReal.message.response.ResponseOracle;
import com.portafolio.turismoReal.model.CheckIn;
import com.portafolio.turismoReal.repository.CheckInRepository;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/checkin")
public class CheckinController {

	
	@Autowired
    CheckInRepository checkinRepository;
	
	 @PostMapping("/create")
	 public ResponseEntity<?> createEquipo(@Valid @RequestBody CheckIn check) {
		 
		    ResponseOracle response = checkinRepository.create(check);
		 
			return new ResponseEntity<>(response, HttpStatus.OK);
	 }
	 
	 @GetMapping("/get/{id}")
	 public CheckIn getCheckin(@PathVariable long id) {
			
		return checkinRepository.getCheckInPorReserva(id);
	 }
}

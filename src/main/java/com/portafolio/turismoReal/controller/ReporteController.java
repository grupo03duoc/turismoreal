package com.portafolio.turismoReal.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.portafolio.turismoReal.services.GenerarFileService;
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/reporte")
public class ReporteController {
	@Autowired
	GenerarFileService generarFileService;
	
	@GetMapping("/generar/{fechaInicio}/{fechaTermino}/{region}")
	public ResponseEntity<?> GenerarReporte(@PathVariable String fechaInicio,@PathVariable String fechaTermino,@PathVariable long region) {
		try {
			ByteArrayInputStream in = generarFileService.GenerarInforme(fechaInicio,fechaTermino,region);
			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Disposition", "attachment; filename=Reporte_TurismoReal.xlsx");
			return ResponseEntity.accepted().headers(headers).body(new InputStreamResource(in));
		} catch (IOException e) {
			e.printStackTrace();
		}

		throw new Error("Error en el Metodo generar Reporte, revisar Logger");
	}
}

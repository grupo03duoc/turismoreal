package com.portafolio.turismoReal.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.portafolio.turismoReal.message.response.ResponseOracle;
import com.portafolio.turismoReal.model.CheckOut;
import com.portafolio.turismoReal.repository.CheckOutRepository;



@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/checkout")
public class CheckoutController {

	@Autowired
    CheckOutRepository checkoutRepository;
	
	@PostMapping("/create")
	 public ResponseEntity<?> createEquipo(@Valid @RequestBody CheckOut check) {
		 
		    ResponseOracle response = checkoutRepository.create(check);
		 
			return new ResponseEntity<>(response, HttpStatus.OK);
	 }
	
	@GetMapping("/get/{id}")
	 public CheckOut getCheckin(@PathVariable long id) {
			
		return checkoutRepository.getCheckInPorReserva(id);
	 }
}

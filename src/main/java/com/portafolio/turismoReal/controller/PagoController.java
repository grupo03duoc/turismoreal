package com.portafolio.turismoReal.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.portafolio.turismoReal.message.request.PagoForm;
import com.portafolio.turismoReal.message.response.ResponseOracle;
import com.portafolio.turismoReal.model.Pago;
import com.portafolio.turismoReal.repository.PagoRepository;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/pago")
public class PagoController {

	@Autowired
    PagoRepository pagoRepository;
	
	@PostMapping("/create")
	public ResponseEntity<?> create(@Valid @RequestBody PagoForm pago) {
		 
		 ResponseOracle response = pagoRepository.create(pago);
		 
		 return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@GetMapping("/get/{id}")
	public Pago getPagoPorReserva(@PathVariable long id) {
		return pagoRepository.getPagoPorReserva(id);
	}
	
	
}

package com.portafolio.turismoReal.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.portafolio.turismoReal.services.IUploadFileService;
import com.portafolio.turismoReal.message.request.DepaEquipamientoForm;
import com.portafolio.turismoReal.message.request.DepartamentoForm;
import com.portafolio.turismoReal.message.response.ResponseOracle;
import com.portafolio.turismoReal.model.Departamento;
import com.portafolio.turismoReal.model.Foto;
import com.portafolio.turismoReal.repository.DepartamentoRepository;
import com.portafolio.turismoReal.repository.EquipamientoRepository;
import com.portafolio.turismoReal.repository.FotoRepository;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/departamento")
public class DepartamentoController {

	@Autowired
    EquipamientoRepository equipamientoRepository;
	
	@Autowired
    DepartamentoRepository departamentoRepository;	
	
	@Autowired
	IUploadFileService uploadService;
	
	@Autowired
    FotoRepository fotoRepository;
	
	
	
	@PostMapping("/create")
	public ResponseEntity<?> createDepa(@Valid @RequestBody DepartamentoForm depa) {
		 
		 ResponseOracle response = departamentoRepository.create(depa);
		 
		 return new ResponseEntity<>(response, HttpStatus.OK);
	}
	 
	 
	@GetMapping("/all/{idComuna}")
	public List<Departamento> getAllDepartamentos(@PathVariable Long idComuna) {
		
	    List<Departamento> depas = new ArrayList<>();
	    depas = departamentoRepository.AllDepartamentos(idComuna);
	    for(Departamento depa : depas) {
	    	depa.setFotos(fotoRepository.allFotos(depa.getId()));
	    	depa.setEquipamientos(equipamientoRepository.allEquipamientoByDep(depa.getId()));
	    }
	    
	    return depas;
	}
	
	@GetMapping("/get/{id}")
	public Departamento getDepartamento(@PathVariable long id) {
		Departamento depa = new Departamento();
		depa = departamentoRepository.getDepartamentoById(id);
		if(depa.getId() != 0 || depa.getId() != null ) {
			depa.setEquipamientos(equipamientoRepository.allEquipamientoByDep(depa.getId()));
		}
		return depa;
	}
	
	@PutMapping("/update")
	public ResponseOracle updateDepartamento(@Valid @RequestBody DepartamentoForm depa) {
		return departamentoRepository.updateDepartamento(depa);
	}
	
	@Transactional
	@GetMapping("/disable/{id}/{estado}")
	public ResponseEntity<?> disableUsuario(@PathVariable long id,@PathVariable int estado) {
		 ResponseOracle response = departamentoRepository.desactivar(id, estado);
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PostMapping("/foto/upload")
	public ResponseEntity<?> uploadFotos(@RequestParam("archivo") MultipartFile archivo,@RequestParam("id") long id){
		ResponseOracle response = new ResponseOracle();
		if(!archivo.isEmpty()) {
			String nombrefoto = null;
			try {
				 nombrefoto = uploadService.copiar(archivo);
				 Foto foto = new Foto();
				 foto.setRuta(nombrefoto);
				 foto.setIdDepartamento(id);
				 
				 response = fotoRepository.create(foto);
				 
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@GetMapping("/obtener/img/{nombreFoto:.+}")
	public ResponseEntity<Resource> verFoto(@PathVariable String nombreFoto){

		Resource recurso = null;
		
		try {
			recurso = uploadService.cargar(nombreFoto);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		HttpHeaders cabecera = new HttpHeaders();
		cabecera.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + recurso.getFilename() + "\"");
		
		return new ResponseEntity<Resource>(recurso, cabecera, HttpStatus.OK);
	}
	
	
	@PostMapping("/equipamiento/create")
	public ResponseEntity<?> createEquipamientoDepartamento( @RequestBody DepaEquipamientoForm depa) {

		ResponseOracle response  = departamentoRepository.createDepaEquipo(depa.getId(), depa.getIdEquipamiento());
		
		 return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	
	
	
}

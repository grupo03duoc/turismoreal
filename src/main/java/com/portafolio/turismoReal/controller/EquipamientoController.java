package com.portafolio.turismoReal.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.portafolio.turismoReal.message.response.ResponseOracle;
import com.portafolio.turismoReal.model.Equipamiento;

import com.portafolio.turismoReal.repository.EquipamientoRepository;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/equipamiento")
public class EquipamientoController {

	@Autowired
    EquipamientoRepository equipamientoRepository;
	
	
	
	 @PostMapping("/create")
	 public ResponseEntity<?> createEquipo(@Valid @RequestBody Equipamiento equipo) {
		 
		    ResponseOracle response = equipamientoRepository.create(equipo);
		 
			return new ResponseEntity<>(response, HttpStatus.OK);
	 }
	 
	 
	 @GetMapping("/all")
	 public List<Equipamiento> getAllEquipamientos() {
			
		 return equipamientoRepository.allEquipamiento();
	 }
	 
	 @GetMapping("/delete/{id}")
	 public ResponseOracle delete(@PathVariable Long id) {
		 
		 ResponseOracle response =equipamientoRepository.delete(id);
		 return response;
	 }
	 
	 @GetMapping("/getAll/{idDepartamento}")
	 public List<Equipamiento> getAllEquipamientosPorDepa(@PathVariable Long idDepartamento) {

		 return equipamientoRepository.allEquipamientoByDep(idDepartamento);
	 }
	
}

package com.portafolio.turismoReal.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.portafolio.turismoReal.message.request.TrasladoForm;
import com.portafolio.turismoReal.message.response.ResponseOracle;
import com.portafolio.turismoReal.repository.TrasladoRepository;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/traslado")
public class TrasladoController {

	@Autowired
    TrasladoRepository trasladoRepository;
	
	@PostMapping("/create")
	public ResponseEntity<?> create(@Valid @RequestBody TrasladoForm traslado) {
		 
		 ResponseOracle response = trasladoRepository.create(traslado);
		 
		 return new ResponseEntity<>(response, HttpStatus.OK);
	}
}

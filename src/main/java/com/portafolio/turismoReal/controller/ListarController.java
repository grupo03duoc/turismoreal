package com.portafolio.turismoReal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.portafolio.turismoReal.model.Comuna;
import com.portafolio.turismoReal.model.Provincia;
import com.portafolio.turismoReal.model.Region;
import com.portafolio.turismoReal.repository.ComunaRepository;
import com.portafolio.turismoReal.repository.ProvinciaRepository;
import com.portafolio.turismoReal.repository.RegionRepository;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/lista")
public class ListarController {

	
	@Autowired
    RegionRepository regionRepository;
	
	@Autowired
    ProvinciaRepository provinciaRepository;
	
	@Autowired
    ComunaRepository comunaRepository;
	
	
	@GetMapping("/regiones")
	public List<Region> getAllRegiones() {
  
	  return  regionRepository.AllRegiones();
		
	}
	
	@GetMapping("/provincias/{idRegion}")
	public List<Provincia> getAllProvincias(@PathVariable Long idRegion) {
  
	  return  provinciaRepository.AllProvincias(idRegion);
	
	}
	
	@GetMapping("/comunas/{idProvincia}")
	public List<Comuna> getAllComunas(@PathVariable Long idProvincia) {
  
	  return  comunaRepository.AllComunas(idProvincia);
		
	}

}

package com.portafolio.turismoReal.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.portafolio.turismoReal.message.response.ResponseOracle;
import com.portafolio.turismoReal.model.Reserva;
import com.portafolio.turismoReal.repository.ReservaRepository;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/reserva")
public class ReservaController {

	@Autowired
    ReservaRepository reservaRepository;
	
	@PostMapping("/create")
	public ResponseEntity<?> create(@Valid @RequestBody Reserva reserva) {
		 
		 ResponseOracle response = reservaRepository.create(reserva);
		 
		 return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@GetMapping("/getAllPorUsuario/{idUsuario}")
	public List<Reserva> getReservaPorUsuario(@PathVariable Long idUsuario) {
		List<Reserva> reservas = new ArrayList<>();
		reservas = reservaRepository.getAllReservaUsuario(idUsuario);
		
		return reservas;
	}
	
	@GetMapping("/getAllPorDepartamento/{id}")
	public List<Reserva> getReservaPorDepartamento(@PathVariable long id) {
		return reservaRepository.getAllReservaDepartamento(id);
	}
	
	@GetMapping("/getAll")
	public List<Reserva> getAllReserva() {
		List<Reserva> reservas = new ArrayList<>();
		reservas = reservaRepository.getAll();
		
		return reservas;
	}
	
	
	@GetMapping("/get/{id}")
	public Reserva getAllReserva(@PathVariable Long id) {
		Reserva reserva = new Reserva();
		reserva = reservaRepository.getReserva(id);
		
		return reserva;
	}
}

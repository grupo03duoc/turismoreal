package com.portafolio.turismoReal.security;

public final class SecurityConstants {

	
    // Se creo la llave en http://www.allkeysgenerator.com/
    public static final String JWT_SECRET = "@NcRfUjXn2r5u8x/A?D(G+KbPdSgVkYp3s6v9y$B&E)H@McQfThWmZq4t7w!z%C*F-JaNdRgUkXn2r5u8x/A?D(G+KbPeShVmYq3s6v9y$B&E)H@McQfTjWnZr4u7w!z";

    // JWT token defaults
    public static final String TOKEN_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String TOKEN_TYPE = "JWT";
    public static final String TOKEN_ISSUER = "kbi-api";
    public static final String TOKEN_AUDIENCE = "kbi-app";
    public static final long VALIDITY_MILLISECONDS = 3600000; //1H
}

package com.portafolio.turismoReal.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.portafolio.turismoReal.model.Usuario;
//import com.portafolio.turismoReal.repository.UsuarioRepository;
import com.portafolio.turismoReal.repository.UsuarioRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	UsuarioRepository usuarioRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
			Usuario user = usuarioRepository.findbyUsername(username);
         	return UserPrinciple.build(user);
		
	}

}